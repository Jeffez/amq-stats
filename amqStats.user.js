// ==UserScript==
// @name         AMQ Stats
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://animemusicquiz.com/
// @icon         https://www.google.com/s2/favicons?domain=animemusicquiz.com
// @downloadURL  https://gitlab.com/Jeffez/amq-stats/-/raw/main/amqStats.user.js
// @updateURL    https://gitlab.com/Jeffez/amq-stats/-/raw/main/amqStats.user.js
// @require      https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

let lastName;
let score;
let check;

var waitForEl = function(selector, callback) {
  if (jQuery(selector).length) {
    callback();
  } else {
    setTimeout(function() {
      waitForEl(selector, callback);
    }, 100);
  }
};

function checkAnime(){
    if ($('#qpAnimeName').text() != lastName){
        lastName = $('#qpAnimeName').text();
        checkAnswer(lastName + $("#qpSongName").text() + $("#qpSongTyoe").text())
    }

}

function checkAnswer(name){
    $('.qpScoreBoardEntry').each(function(i){
        if($(this).find(".qpsPlayerName").hasClass("self")){
            var playerName = $(this).find('.qpsPlayerName').text();
            console.log("Updating : "+playerName);
            var count = GM_getValue(name+"count");
            if(count !== parseInt(count, 10)){count = 0}
            console.log(count);
            var correct = GM_getValue(name+"correct");
            if(correct !== parseInt(correct, 10)){correct = 0}
            console.log(correct);
            count++;
            if(!$(this).find('qpsPlayerScore').attr('data-content') === "Points"){
                if (score != $(this).find('.qpsPlayerGuessCount').text()){
                    score = $(this).find('.qpsPlayerGuessCount').text()
                    correct++;
                }
            }else {
                if (score != $(this).find('.qpsPlayerScore').text()){
                    score = $(this).find('.qpsPlayerScore').text()
                    correct++;
                }
            }
            updatePercentage(count,correct);
            console.log(correct + "/" + count);
            console.log(name + ":" + Math.round((correct/count)*100));
            GM_setValue(name+"count",count);
            GM_setValue(name+"correct",correct);
        }
    })
}

function updatePercentage(count, correct){
    $("#qpSongPercentage").text(Math.round((correct/count)*100)+"% - ("+count+")");
}

function initialize(){
    lastName = "-";
    $('#qpAnimeName').text("-");
    score = 0;
    console.log("Initializing");
    clearInterval(check);
    waitForEl(".qpScoreBoardEntry", function() {
        check = setInterval(checkAnime, 1000);
    });
    waitForEl("#qpReportFeedbackContainer", function(){
        $("#qpSongPercentage").remove();
        var firstChild = $("#qpReportFeedbackContainer").parent().children().eq(0);
        firstChild.after('<div class="row"><b id="qpSongPercentage">?% - (?)</b></div>');
        firstChild.find("#qpSongPercentage").text();
    });
}



waitForEl("#lbStartButton", function() {
    $('#lbStartButton').on("click",initialize);
});
