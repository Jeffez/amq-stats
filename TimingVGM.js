// ==UserScript==
// @name         Timing VGM-Quiz
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.vgm-quiz.com/play/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=vgm-quiz.com
// @grant        none
// ==/UserScript==

console.stdlog = console.log.bind(console);
console.logs = [];
console.log = function(){
    document.getElementsByClassName("goodanswer")[document.getElementsByClassName("goodanswer").length-1].textContent += " " + arguments[0]["chrono"] + "ms"
    console.stdlog.apply(console, arguments);
}
